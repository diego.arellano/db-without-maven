import java.sql.*;

public class test {
    public static void main( String[] args ){
        String url = "jdbc:postgresql://localhost:5432/dbs2";
        String user = "user";
        String password = "pwd";

        try {
            Connection con = DriverManager.getConnection(url, user, password);
            con.setAutoCommit(false);
            PreparedStatement ps = con.prepareStatement(
                "INSERT INTO freundschaft (uid1, uid2, fgrad) VALUES (?,?,?)"
            );
            ps.setInt(1, 2);
            ps.setInt(2, 4);
            ps.setString(3, "Freunde");
            ps.executeUpdate();

            ps.close();
            con.commit();
            con.close();

        }  catch (SQLException e){
            System.err.println("Something went wrong.");
            e.printStackTrace();
        }
    }
}
