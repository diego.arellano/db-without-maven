# Simple JDBC Application

Based on https://www.ibm.com/docs/en/db2/11.1?topic=SSEPGG_11.1.0/com.ibm.db2.luw.apdv.java.doc/src/tpc/imjcc_cjvjdbas.htm

This subproject is for connecting and working with a PostregSQL Database in Java without using Maven or Spring.

## Database
    CREATE TABLE Nutzer (
        uid     SERIAL PRIMARY KEY,
        email   VARCHAR(50),
        name    VARCHAR(100)
    );

    CREATE TABLE Freundschaft (
        uid1    INT NOT NULL,
        uid2    INT NOT NULL,
        Fgrad   VARCHAR(100),
        PRIMARY KEY( uid1, uid2 ),
        FOREIGN KEY (uid1) REFERENCES Nutzer (uid),
        FOREIGN KEY (uid2) REFERENCES Nutzer (uid)
    );