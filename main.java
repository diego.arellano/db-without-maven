import java.sql.*;
import java.util.Scanner;

/*
 * Simple JDBC application.
 * You can connect to the database, see and edit its entries and add new entries to it.
 */
public class main {
    static String url = "jdbc:postgresql://localhost:5432/dbs2";
    static String user = "user";
    static String password = "pwd";
    static Connection con;

    public static void main(String[] args) {

        System.out.println("Welcome to this small database.\n" +
            "What would you like to do?\n" +
            "1: Show all Users.\n" +
            "2: Add new User.\n" +
            "3: Edit existing User.\n" +
            "4: Show all Friendships.\n" +
            "5: Show all Friendships of a User.\n" +
            "6: Add new Friendship.\n" +
            "7: Edit a Friendship."
        );

        Scanner scanner = new Scanner(System.in);
        int antwort;
        try {
            antwort = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e){
            antwort = 0;
        }

        if (antwort == 1){
            listAllNutzer();
        } else if (antwort == 2) {
            System.out.println("Please input name and email from new User:");
            String name = scanner.nextLine();
            String email = scanner.nextLine();
            if (name != null && !name.isEmpty()
                    && email != null && !email.isEmpty()
            ) {
                addNutzer(name, email);
            }
        } else if (antwort == 3) {
            System.out.println("Do you know the name (1) or uid (2) of the user?");
            try {
                antwort = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("Wrong answer.");
            }
            if (antwort == 1) {
                System.out.println("Please input their name:");
                String name = scanner.nextLine();
                if (name != null && !name.isEmpty()) {
                    updateNutzer(null, name);
                }
            } else if (antwort == 2) {
                System.out.println("Please input their uid:");
                int uid = 0;
                try {
                    uid = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    System.err.println("Wrong input.");
                }
                updateNutzer(uid, null);
            }
        } else if (antwort == 4){
            seeAllFreundschaften();
        } else if (antwort == 5) {
            System.out.println("Do you know the name (1) or uid (2) of the user?");
            try {
                antwort = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("Wrong answer.");
            }
            if (antwort == 1) {
                System.out.println("Please input their name:");
                String name = scanner.nextLine();
                if (name != null && !name.isEmpty()) {
                    seeAllFreundschaftenByUidOrName(null, name);
                }
            } else if (antwort == 2) {
                System.out.println("Please input their uid:");
                int uid = 0;
                try {
                    uid = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    System.err.println("Wrong input.");
                }
                seeAllFreundschaftenByUidOrName(uid, null);
            }
        } else if (antwort == 6) {
            System.out.println("Please input the ids from the Users and their relationship:");
            int uid1 = 0, uid2 = 0;
            String fGrad;
            try {
                uid1 = Integer.parseInt(scanner.nextLine());
                uid2 = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("There was something wrong with the input.");
            }
            fGrad = scanner.nextLine();
            if (fGrad != null && !fGrad.isEmpty()) {
                addFreundschaft(uid1, uid2, fGrad);
            }
        } else if (antwort == 7){
            System.out.println("Do you know the names (1) or uids (2) of the users?");
            try {
                antwort = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("Wrong answer.");
            }
            if (antwort == 1) {
                System.out.println("Please input their names:");
                String name1 = scanner.nextLine();
                String name2 = scanner.nextLine();
                if (!name1.isEmpty() && !name2.isEmpty()) {
                    editFreundschaft(null, null, name1, name2);
                }
            } else if (antwort == 2) {
                System.out.println("Please input their uids:");
                int uid1 = 0, uid2 = 0;
                try {
                    uid1 = Integer.parseInt(scanner.nextLine());
                    uid2 = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    System.err.println("Wrong input.");
                }
                editFreundschaft(uid1, uid2, null, null);
            }

        } else {
            System.out.println("Goodbye.");
        }

    }

    public static void connectDB(boolean autocommit){
        try {
            con = DriverManager.getConnection(url, user, password);
            con.setAutoCommit(autocommit);
//            System.out.println("Created a JDBC connection to the data source");
        } catch (SQLException e) {
            System.err.println("Couldn't connect to database.");
            e.printStackTrace();
        }
    }

    public static void disconnectDB(){
        try {
            if (!con.getAutoCommit()){
                con.commit();
            }
            con.close();

        } catch (SQLException e){
            System.err.println("Couldn't disconnect from Database.");
            e.printStackTrace();
        }
    }

    public static void listAllNutzer(){
        try {
            connectDB(false);
            con.setReadOnly(true);
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT uid, name, email FROM Nutzer");

            while (rs.next()){
                System.out.println(rs.getString(1) + ", " + rs.getString(2)
                    + ", " + rs.getString(3));
            }
            rs.close();
            stmt.close();
            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't list all Nutzer.");
            e.printStackTrace();
        }
    }

    public static int getNutzerUid(String name){
        int uid = 0;
        try {
            connectDB(false);
            con.setReadOnly(true);
            String sql = "SELECT name, count(*) FROM Nutzer " +
                    "WHERE name = ? GROUP BY name";
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setString(1, name);
            ResultSet rs = pStmt.executeQuery();

            if (rs.next()){
                if (rs.getInt(2) != 1){
                    System.out.println("Error: There is more than one User with the same name.");
                } else {
                    sql = "SELECT uid FROM Nutzer WHERE name = ?";
                    pStmt = con.prepareStatement(sql);
                    pStmt.setString(1, name);
                    ResultSet rs2 = pStmt.executeQuery();
                    if (rs2.next()){
                        uid = rs2.getInt(1);
                    }
                    rs2.close();
                }
            }

            rs.close();
            pStmt.close();
            disconnectDB();

        } catch (SQLException e){
            System.err.println("Couldn't find User.");
            e.printStackTrace();
        }

        return uid;

    }

    public static String getUserName(int uid){
        String name = "";
        try {
            connectDB(false);
            con.setReadOnly(true);
            String sql = "SELECT name FROM Nutzer N " +
                "WHERE N.uid = ?";
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setInt(1, uid);

            ResultSet rs = pStmt.executeQuery(sql);
            if (rs.next()){
                name = rs.getString(1);
            }

            rs.close();
            pStmt.close();
            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't find User by uid.");
            e.printStackTrace();
        }

        return name;
    }

    public static void addNutzer(String name, String email){
        try {
            connectDB(false);
            PreparedStatement pStmt = con.prepareStatement("INSERT INTO Nutzer (email, name) VALUES (?,?)");
            pStmt.setString(1, email);
            pStmt.setString(2, name);
            pStmt.executeUpdate();

            pStmt.close();
            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't add new User.");
            e.printStackTrace();
        }
    }

    public static boolean updateNutzer(Integer uid, String oldName){
        if (uid == null){
            uid = getNutzerUid(oldName);
            if (uid == 0){
                System.out.println("Error: Cannot update User, there are more than one user with the same name.");
                return false;
            }
        }

        System.out.println("Input the new name and new email for the user in question.\n" +
            "Hint: if there are no changes leave the input blank."
        );
        Scanner scanner = new Scanner(System.in);
        String newName = scanner.nextLine();
        String newEmail = scanner.nextLine();

        try {
            connectDB(false);

            String sql;
            if (newName.isEmpty()){
                sql = "UPDATE Nutzer SET email = ? WHERE uid = ?";
                PreparedStatement pStmt = con.prepareStatement(sql);
                pStmt.setString(1, newEmail);
                pStmt.setInt(2, uid);
                pStmt.executeUpdate();
                pStmt.close();
            } else if (newEmail.isEmpty()){
                sql = "UPDATE Nutzer SET name = ? WHERE uid = ?";
                PreparedStatement pStmt = con.prepareStatement(sql);
                pStmt.setString(1, newName);
                pStmt.setInt(2, uid);
                pStmt.executeUpdate();
                pStmt.close();
            } else {
                sql= "UPDATE Nutzer SET name = ?, email = ? " +
                        "WHERE uid = ?";
                PreparedStatement pStmt = con.prepareStatement(sql);
                pStmt.setString(1, newName);
                pStmt.setString(2, newEmail);
                pStmt.setInt(3, uid);
                pStmt.executeUpdate();
                pStmt.close();
            }

            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't update User.");
            e.printStackTrace();
        }

        return true;
    }

    public static void addFreundschaft(int uid1, int uid2, String fGrad){
        String sql = "INSERT INTO freundschaft (uid1, uid2, fgrad) VALUES (?,?,?)";
        try {
            connectDB(false);
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setInt(1, uid1);
            pStmt.setInt(2, uid2);
            pStmt.setString(3, fGrad);
            pStmt.executeUpdate();

            pStmt.close();
            disconnectDB();

        } catch (SQLException e){
            System.err.println("Couldn't update Relationship.");
            e.printStackTrace();
        }
    }

    public static void seeAllFreundschaften(){
        String sql = "SELECT uid1, uid2, Fgrad FROM Freundschaft";

        try {
            connectDB(false);
            con.setReadOnly(true);
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()){
                System.out.println(rs.getInt(1) + ", " +
                rs.getInt(2) + ": " + rs.getString(3) );
            }

            rs.close();
            st.close();
            disconnectDB();

        } catch (SQLException e){
            System.err.println("Couldn't list Relationships.");
            e.printStackTrace();
        }
    }

    public static boolean seeAllFreundschaftenByUidOrName(Integer uid, String name){
        if (uid == null){
            uid = getNutzerUid(name);
            if (uid == 0){
                System.out.println("Error: Cannot find User, there are more than one user with the same name.");
                return false;
            }
        }

        if (name == null){
            name = getUserName(uid);
        }

        String sql = "WITH aux AS (" +
            "  SELECT uid, name" +
            "  FROM Nutzer" +
            "  WHERE uid = ? " +
            ")" +
            "SELECT aux.name, N2.Name, F.Fgrad " +
            "FROM aux, Nutzer N2, Freundschaft F " +
            "WHERE (aux.uid = F.uid1 AND N2.uid = F.uid2) " +
            "OR (aux.uid = F.uid2 AND N2.uid = F.uid1)";

        try {
            connectDB(false);
            con.setReadOnly(true);
            PreparedStatement pStmt = con.prepareStatement(sql);
            pStmt.setInt(1, uid);

            ResultSet rs = pStmt.executeQuery();
            System.out.println("Relationships from User " + name + ":");
            while (rs.next()){
                System.out.println(rs.getString(1)  +
                    ", " + rs.getString(2) +
                    ": " + rs.getString(3));
            }

            rs.close();
            pStmt.close();
            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't realize transaction.");
            e.printStackTrace();
        }

        return true;
    }

    public static boolean editFreundschaft(Integer uid1, Integer uid2, String name1, String name2){
        if (uid1 == null){
            uid1 = getNutzerUid(name1);
            uid2 = getNutzerUid(name2);
            if (uid1 == 0 || uid2 == 0){
                System.out.println("Error: at least one of the Users couldn't be found by name.");
                return false;
            }
        }

        System.out.println("Please input the new relationship:");
        Scanner scanner = new Scanner(System.in);
        String neuFgrad = scanner.nextLine();
        if (neuFgrad.isEmpty()){
            System.out.println("Error: new relationship cannot be empty.");
            return false;
        }

        String sql = "UPDATE Freundschaft SET Fgrad = ? WHERE (uid1 = ? AND uid2 = ?) OR (uid1 = ? AND uid2 = ?)";

        try {
            connectDB(false);
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, neuFgrad);
            ps.setInt(2, uid1);
            ps.setInt(3, uid2);
            ps.setInt(4, uid2);
            ps.setInt(5, uid1);
            ps.executeUpdate();
            ps.close();
            disconnectDB();
        } catch (SQLException e){
            System.err.println("Couldn't realize transaction.");
            e.printStackTrace();
        }

        return true;
    }
}
