import java.sql.*;

/*
 * Simple JDBC Application.
 * First attempt at connecting with a database without maven or spring.
 * Source: https://www.ibm.com/docs/en/db2/11.1?topic=SSEPGG_11.1.0/com.ibm.db2.luw.apdv.java.doc/src/tpc/imjcc_cjvjdbas.htm
 */

public class Old {
    String urlPrefix = "";
    static String url = "jdbc:postgresql://localhost:5432/dbs2";
    static String user = "user";
    static String password = "pwd";
    static String uid;
    static Connection con;
    static Statement stmt;
    static ResultSet rs;

    public static void main(String[] args){
        try {
//            Class.forName("org.postgresql.Driver");
//            System.out.println("Loaded JDBC Driver.");

            con = DriverManager.getConnection(url, user, password);
            con.setAutoCommit(false);
            System.out.println("Created a JDBC connection to the data source");

            stmt = con.createStatement();
            System.out.println("Created JDBC Statement object");

            rs = stmt.executeQuery("SELECT uid FROM Nutzer");
            System.out.println("Created JDBC ResultSet object");

            while (rs.next()){
                uid = rs.getString(1);
                System.out.println("Nutzer number: " + uid);
            }
            System.out.println("Fetched all rows from JDBC ResultSet");


            System.out.println("Closed JDBC ResultSet");

            stmt.close();
            System.out.println("Closed JDBC Statement");

            con.commit();
            System.out.println("Transaction commited");

            con.close();
            System.out.println("Disconnected from data source");

//        } catch (ClassNotFoundException e){
//            System.err.println("Could not loead JDBC driver");
//            e.printStackTrace();

        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
}
